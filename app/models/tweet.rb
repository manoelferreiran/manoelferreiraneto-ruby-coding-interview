class Tweet < ApplicationRecord
  def self.paginated(current_page = 1, page_size = 5, reference_id)
    Tweet.where('OFFSET ? ROWS FETCH NEXT ? ROWS ONLY', "#{(current_page - 1) * page_size}", page_size)
  end
end
